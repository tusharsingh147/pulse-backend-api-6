package com.healthpulse.backend.api.service;

import com.healthpulse.backend.api.configuration.AppProperties;
import com.healthpulse.backend.api.model.*;
import com.healthpulse.backend.api.utils.RsaUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class AuthService {

    private final AppProperties appProperties;
    private RestTemplate restTemplate = new RestTemplate();
    public static AuthToken authToken;

    private AuthToken getToken() throws URISyntaxException {

        URI uri = new URI(appProperties.getSession());

        // create headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // request body parameters
        Map<String, Object> map = new HashMap<>();
        map.put("clientId", "SBX_000672");
        map.put("clientSecret", "6a87dad1-b91a-40d5-8391-b2630c532453");

        // build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        ResponseEntity<AuthToken> result = restTemplate.postForEntity(uri, entity, AuthToken.class);

        // check response
//        if (result.getStatusCode() == HttpStatus.OK) {
//            System.out.println("Request Successful");
//            System.out.println(response.getBody());
//        } else {
//            System.out.println("Request Failed");
//            System.out.println(response.getStatusCode());
//        }
        authToken = result.getBody();
        authToken.setTimestamp(LocalDateTime.now());


        return authToken;
    }

    public GenerateOtpResponse generateOtp(GenerateOtpRequest request, String type) throws URISyntaxException {
        if(type.equalsIgnoreCase("pulse")){
            return new GenerateOtpResponse("123456");
        }

        else{
            URI uri = new URI(appProperties.getBaseUrl() + "/registration/"+ type + "/generateOtp");

            this.getToken();
            // create headers
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setBearerAuth(authToken.getAccessToken());

            // request body parameters
            Map<String, Object> body = new HashMap<>();
            body.put(type, RsaUtil.getEncryptedString(request.getValue()));

            // build the request
            HttpEntity<Map<String, Object>> entity = new HttpEntity<>(body, headers);
            ResponseEntity<GenerateOtpResponse> result = restTemplate.postForEntity(uri, entity, GenerateOtpResponse.class);
            return result.getBody();
        }

    }

    public String verifyOtp(VerifyOtpRequest request, String type) throws URISyntaxException {
        if(type.equalsIgnoreCase("pulse")){
            if(request.getOtp().equalsIgnoreCase("1234")){
                return "Verify Success";
            }
            else throw new InputMismatchException("Otp Mismatch");
        }
        else {
            URI uri = new URI(appProperties.getBaseUrl() + "/registration/"+ type + "/verifyOTP");

            this.getToken();
            // create headers
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(authToken.getAccessToken());
            // request body parameters
            //Map<String, Object> map = new HashMap<>();
            //map.put(request.getType(), request.getValue());
            request.setOtp(RsaUtil.getEncryptedString(request.getOtp()));
            // build the request
            HttpEntity<VerifyOtpRequest> entity = new HttpEntity<>(request, headers);
            ResponseEntity<String> result = restTemplate.postForEntity(uri, entity, String.class);
            return result.getBody();
        }
    }

    public String resendOtp(VerifyOtpRequest request, String type) throws URISyntaxException {
        URI uri = new URI(appProperties.getBaseUrl() + "/registration/"+ type + "/resendAadhaarOtp");
        // create headers
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(authToken.getAccessToken());
        // request body parameters
        Map<String, Object> body = new HashMap<>();
        body.put("txnId", request.getTxnId());
        // build the request
        HttpEntity<VerifyOtpRequest> entity = new HttpEntity<>(request, headers);
        //HttpEntity<Map<String, Object>> entity = new HttpEntity<>(body, headers);
        ResponseEntity<String> result = restTemplate.postForEntity(uri, entity, String.class);
        return result.getBody();
    }

    public HealthIdResponse createHealthId(HealthIdRequest request, String type) throws URISyntaxException {
        URI uri = new URI(appProperties.getBaseUrl() + "/registration/"+ type + "/createHealthId");

        this.getToken();
        // create headers
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(authToken.getAccessToken());

        // build the request
        HttpEntity<HealthIdRequest> entity = new HttpEntity<>(request, headers);
        ResponseEntity<HealthIdResponse> result = restTemplate.postForEntity(uri, entity, HealthIdResponse.class);
        return result.getBody();
    }
}

