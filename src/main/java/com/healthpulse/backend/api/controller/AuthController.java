package com.healthpulse.backend.api.controller;

import com.healthpulse.backend.api.model.GenerateOtpRequest;
import com.healthpulse.backend.api.model.HealthIdRequest;
import com.healthpulse.backend.api.model.VerifyOtpRequest;
import com.healthpulse.backend.api.service.AuthService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1", produces = APPLICATION_JSON_VALUE)
@Tag(name = "Auth API", description = "Authentication Authorisation API for Pulse Users")
public class AuthController {

    private final AuthService authService;

    @GetMapping("/test")
    public ResponseEntity<?> test() {
        return ResponseEntity.ok("all good !!");
    }


    @PostMapping("/registration/generateOtp")
    @Operation(summary="Generate OTP")
    public ResponseEntity<?> generateOtp(@RequestBody GenerateOtpRequest request, @RequestHeader("mode") String type) throws URISyntaxException {
        return ResponseEntity.ok(authService.generateOtp(request,type));
    }

    @PostMapping("/registration/verifyOtp")
    @Operation(summary="Verify OTP")
    public ResponseEntity<?> verifyOtp(@RequestBody VerifyOtpRequest request, @RequestHeader("mode") String type) throws URISyntaxException {
        return ResponseEntity.ok(authService.verifyOtp(request,type));
    }

    @PostMapping("/registration/resendOtp")
    @Operation(summary="Resend OTP")
    public ResponseEntity<?> resendOtp(@RequestBody VerifyOtpRequest request, @RequestHeader("mode") String type) throws URISyntaxException {
        return ResponseEntity.ok(authService.resendOtp(request,type));
    }

    @PostMapping("/registration/createId")
    @Operation(summary="Create Health Id")
    public ResponseEntity<?> createHealthId(@RequestBody HealthIdRequest request, @RequestHeader("mode") String type) throws URISyntaxException {
        return ResponseEntity.ok(authService.createHealthId(request,type));
    }
}
